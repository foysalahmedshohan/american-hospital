<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');          
            $table->string('npackage');
            $table->string('tshared');
            $table->string('sstandard')->nullable();
            $table->string('sdelux');
            $table->string('image')->nullable();
            $table->string('suit');
            $table->string('dhour');
            $table->string('remarks');
            $table->string('echarge');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
