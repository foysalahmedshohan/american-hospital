@extends('admin.layout.master')


@section('content')


<div style="margin-left: 190px;" class="container" style="width: 100%;">
	<h2 style="margin-left: 408px; margin-top: 80px;">Add Doctor Profile</h2>
	<hr>
	
  
 	 	<div class="row">
 	 		

 	 		<form method="post" action="{{route('doctor.store')}}" enctype="multipart/form-data">
        	
            {{csrf_field()}}
	    <div class="col-md-6">

			<div class="form-group">
				  <label for="user">Name:</label>
				  <input type="text" class="form-control" id="Name" name="name">
			</div> 

				    
		            
		    

		    <div class="form-group">
				  <label for="user">Email:</label>
				  <input type="email" class="form-control" id="Email" name="email">
			</div>

         	<div class="form-group">
				  <label for="user">Address:</label>
				  <input type="address" class="form-control" id="address" name="address">
			</div>

              

			
			<div class="form-group">
				  <label for="user">Specialities:</label>
				  <input type="text" class="form-control" id="Specilities" name="specilities">
			</div>

			<div class="form-group">
				  <label for="user">Designation:</label>
				  <input type="text" class="form-control" id="Name" name="designation">
			</div> 

			
			

			 <div class="btn-group">
         	<br>
			  <button type="submit" class="btn btn-success">Submit</button>
			  <button type="reset" class="btn btn-danger">Reset</button>
				  
		    </div>

		</div>


        
		<div class="col-md-6">



			<div class="form-group">
				  <label for="user">Birth Date:</label>
				  <input type="date" class="form-control" id="Date" name="bd">
			</div>		 	

			 <div class="form-group">
				  <label for="user">Degree:</label>
				  <input type="text" class="form-control" id="Degree" name="degree">
			</div>

			 <div class="form-group">
				  <label for="user">Phone:</label>
				  <input type="number" class="form-control" id="Degree" name="phone">
			</div>

			<div class="form-group">
				  <label for="user">Schedule:</label>
				  <input type="text" class="form-control" id="Name" name="schedule">
			</div>  

			<div class="form-group">
				  <label for="user">Image:</label>
				  <input type="file"  id="usr" value="{{old('image')}}" name="image">
			</div>   

		
			


		      
		</div>
  		
  		
		</form>

		


		</div>	

</div>			    




@stop