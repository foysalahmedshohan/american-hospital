@extends('admin.layout.master')


@section('content')


<div style="margin-left: 190px;" class="container" style="width: 100%;">
	<h2 style="margin-left: 408px; margin-top: 80px;">Update Doctor Profile</h2>
	<hr>
	
  
 	 	<div class="row">
 	 		

 	 		<form method="post"  action="{{route('doctor.update', $doctor->id )}}" enctype="multipart/form-data">
        	{{csrf_field()}}
 			@method('put')
	    <div class="col-md-6">

			<div class="form-group">
				  <label for="user">Name:</label>
				  <input type="text" class="form-control" id="Name" value="{{$doctor->name}}" name="name">
			</div> 

				    
		            
		    

		    <div class="form-group">
				  <label for="user">Email:</label>
				  <input type="email" class="form-control" id="Email" value="{{$doctor->email}}" name="email">
			</div>

         	<div class="form-group">
				  <label for="user">Address:</label>
				  <input type="address" class="form-control" id="address" value="{{$doctor->address}}" name="address">
			</div>

              

			
			<div class="form-group">
				  <label for="user">Specilities:</label>
				  <input type="text" class="form-control" id="Specilities" value="{{$doctor->specilities}}" name="specilities">
			</div>

			<div class="form-group">
				  <label for="user">Designation:</label>
				  <input type="text" class="form-control" id="Name" value="{{$doctor->designation}}" name="designation">
			</div> 

			
			

			 <div class="btn-group">
         	<br>
			  <button type="submit" class="btn btn-success">Submit</button>
			  <button type="reset" class="btn btn-danger">Reset</button>
			  <button type="submit" class="btn btn-primary">index</button>
				  
		    </div>

		</div>


        
		<div class="col-md-6">



			<div class="form-group">
				  <label for="user">Birth Date:</label>
				  <input type="date" class="form-control" id="Date" value="{{$doctor->bd}}" name="bd">
			</div>		 	

			 <div class="form-group">
				  <label for="user">Degree:</label>
				  <input type="type" class="form-control" id="Degree" value="{{$doctor->degree}}" name="degree">
			</div>

			<div class="form-group">
				  <label for="user">Schedule:</label>
				  <input type="text" class="form-control" id="Name" value="{{$doctor->schedule}}" name="schedule">
			</div> 

			<div class="form-group">
				  <label for="user">Mobile:</label>
				  <input type="text" class="form-control" id="Name" value="{{$doctor->phone}}" name="phone">
			</div> 

			 <div class="form-group">
					<label for="user">Image:</label>
					<img style="width:100px;height:100px" src="{{ asset('image/'.$doctor->image) }}" alt="">
					 <input type="file"  id="usr" name="image">
			</div>

		
			


		      
		</div>
  		
  		
		</form>


		</div>	

</div>			    



@stop