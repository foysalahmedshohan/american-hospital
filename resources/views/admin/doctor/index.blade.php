@extends('admin.layout.master')


@section('content')


<div style="margin-left: 190px;" class="container" style="width: 100%;">
  <h2 style="margin-left: 408px; margin-top: 80px;">Add Doctor Profile</h2>
  <hr>
  
  
    <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                All Doctors.....
              </header>

              <table id="example1" class="table table-striped table-advance table-hover">
                <tbody>
                  <tr>
                    <th><i class="icon_profile"></i> Full Name</th>                   
                    <th><i class="icon_mail_alt"></i> Email</th>
                    <th><i class="icon_pin_alt"></i> City</th>
                    <th><i class="icon_mobile"></i> Mobile</th>
                    <th><i class="icon_mobile"></i> Specialities</th>
                    <th><i class="icon_mobile"></i> schedule</th>
                    <th><i class="icon_cogs"></i> Action</th>
                  </tr>
                  <tr>
                     @if($doctor)
                    @foreach($doctor as $doctor)
                    <td>{{ $doctor->name }}</td>                  
                    <td>{{ $doctor->email }}</td>
                    <td>{{ $doctor->address }}</td>
                    <td>{{ $doctor->phone }}</td>
                    <td>{{ $doctor->specilities }}</td>
                    <td>{{ $doctor->schedule }}</td>
                    <td>
                      <div class="btn-group">
                        <a class="btn btn-primary" href="{{ route('doctor.edit',$doctor->id) }}"><i class="icon_plus_alt2"></i></a>
 
                        <a class="btn btn-success" href="{{route('doctor.create')}}"><i class="icon_check_alt2"></i></a>

                       <form id="delete-form-{{ $doctor->id }}" action="{{ route('doctor.destroy',$doctor->id) }}" style="display: none;" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                    </form>
                    <button type="button" class="btn btn-danger btn" title="Delete" onclick="if(confirm('Are you sure? You want to delete this?')){
                        event.preventDefault();
                        document.getElementById('delete-form-{{ $doctor->id }}').submit();
                    }else {
                        event.preventDefault();
                            }"><i class="icon_close_alt2"></i>
                    </button>

                      </div>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </section>
          </div>
        </div>
       </div>

@stop
@push('script')
<script type="text/javascript">
  $(function () {
    //$('#example1').DataTable();
    $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
</script>
@endpush