@extends('admin.layout.master')


@section('content')


<div class="container" style="width: 100%;">
	<h2 style="margin-left: 30px;">Update Doctor Profile</h2>
	<hr>
	<br>
  
 	 	<div class="row">
 
 	 		 @if(Session::has('update'))
              <div class="">
                <h5 style="color: green; ;margin-left: 15px; margin-top: -1px;"> {{Session::get('update')}}</h5> 
              </div>
              @endif 

 	 		<form method="post" action="{{route('doctor.update', $doctor->id )}}" enctype="multipart/form-data">
        	{{csrf_field()}}
 			@method('put')

	    <div class="col-md-6">

			<div class="form-group">
				  <label for="user">Name:</label>
				  <input type="text" class="form-control" id="Name" value="{{$doctor->name}}" name="name">
			</div>         
		    

		    <div class="form-group">
				  <label for="user">Email:</label>
				  <input type="email" class="form-control" id="Email" value="{{$doctor->email}}" name="email">
			</div>

         	<div class="form-group">
				  <label for="user">Address:</label>
				  <input type="address" class="form-control" id="address" value="{{$doctor->address}}" name="address">
			</div>
              

			<div class="form-group">
				  <label for="user">Phone Number:</label>
				  <input type="number" class="form-control" id="Phone" value="{{$doctor->phone}}" name="phone">
			</div>
			<div class="form-group">
				  <label for="user">Specilities:</label>
				  <input type="text" class="form-control" id="Specilities" value="{{$doctor->specilities}}" name="specilities">
			</div>

			 <div class="btn-group">
         	<br>
			  <button type="submit" class="btn btn-success">Submit</button>
			  <button type="reset" class="btn btn-primary">Reset</button>
				  
		    </div>

		</div>


        
		<div class="col-md-6">

			<div class="form-group">
				  <label for="user">Birth Date:</label>
				  <input type="date" class="form-control" id="Date" value="{{$doctor->bd}}" name="bd">
			</div>		 	

			 <div class="form-group">
				  <label for="user">Degree:</label>
				  <input type="type" class="form-control" id="Degree" value="{{$doctor->degree}}" name="degree">
			</div>

			<div class="form-group">
				  <label for="user">Password:</label>
				  <input type="password" class="form-control" id="pwd" value="{{$doctor->password}}" name="password">
			</div>
			<div class="form-group">
				  <label for="user">Image:</label>
				  <input type="file"  id="usr" value="{{$doctor->image}}"doctor name="image">
			</div>

		      
		</div>
  		
  		
		</form>

		


		</div>	

</div>			    




@stop