@extends('admin.layout.master')


@section('content')


<div style="margin-left: 190px;" class="container" style="width: 100%;">
	<h2 style="margin-left: 408px; margin-top: 80px;">Add Seat & Cost</h2>
	<hr>
	
  
 	 	<div class="row">
 	 		

 	 		<form method="post"  action="{{route('seat.update', $seat->id )}}" enctype="multipart/form-data">
        	
            {{csrf_field()}}
            @method('put')
        	

	    <div class="col-md-6">

			 <div class="form-group">
				  <label for="user">Seat Type</label>
				  <input type="text" class="form-control" id="Name" value="{{$seat->stype}}" name="stype">
			</div> 

			<div class="form-group">
				  <label for="user">Total Seat</label>
				  <input type="text" class="form-control" id="Name" value="{{$seat->tseat}}" name="tseat">
			</div> 
	
			 <div class="form-group">
					<label for="user">Image:</label>
					<img style="width:80px;height:80px" src="{{ asset('image/'.$seat->image) }}" alt="">
					 <input type="file"  id="usr" name="image">
			</div>

			 <div class="btn-group">
         	<br>
			  <button type="submit" class="btn btn-success">Submit</button>
			  <button type="reset" class="btn btn-danger">Reset</button>
				  
		    </div>

		</div>


        
		<div class="col-md-6">

	 	

			 <div class="form-group">
				  <label for="user">Available Seat:</label>
				  <input type="text" class="form-control" id="Degree" value="{{$seat->aseat}}" name="aseat">
			</div>

			<div class="form-group">
				  <label for="user">Charge:</label>
				  <input type="text" class="form-control" id="Degree" value="{{$seat->charge}}" name="charge">
			</div>

			 

		
			


		      
		</div>
  		
  		
		</form>

		


		</div>	

</div>			    




@stop