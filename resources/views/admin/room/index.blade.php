@extends('admin.layout.master')


@section('content')


<div style="margin-left: 190px;" class="container" style="width: 100%;">
  <h2 style="margin-left: 408px; margin-top: 80px;">All Seat Information</h2>
  <hr>
  
  
    <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Advanced Table
              </header>

              <table class="table table-striped table-advance table-hover">
                <tbody>
                  <tr>
                    <th><i class=""></i> Room Type</th>
                    <th><i class=""></i> Total Seat</th>
                    <th><i class=""></i> Available Seat</th>
                    <th><i class=""></i> Charge</th>
                    <th><i class=""></i> Action</th>

                   
                  </tr>
                  <tr>
                     @if($seat)
                    @foreach($seat as $seat)
                    <td>{{ $seat->stype }}</td> 
                    <td>{{ $seat->tseat }}</td>
                  
                    <td>{{ $seat->aseat }}</td>
                    <td>{{ $seat->charge }}</td>
                   
                    <td>
                      <div class="btn-group">

                        <a class="btn btn-primary" href="{{ route('seat.edit',$seat->id) }}"><i class="icon_plus_alt2"></i></a>

                        <a class="btn btn-success" href="#"><i class="icon_check_alt2"></i></a>
                       
                        <form id="delete-form-{{ $seat->id }}" action="{{ route('seat.destroy',$seat->id) }}" style="display: none;" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                    </form>
                    <button type="button" class="btn btn-danger " title="Delete" onclick="if(confirm('Are you sure? You want to delete this?')){
                        event.preventDefault();
                        document.getElementById('delete-form-{{ $seat->id }}').submit();
                    }else {
                        event.preventDefault();
                            }"><i class="icon_close_alt2"></i>
                    </button>

                      </div>
                    </td>
                  </tr>
                 
                      </div>
                    </td>
                  </tr>
                   @endforeach
                  @endif
                </tbody>
              </table>
            </section>
          </div>
        </div>
        </div>

@stop