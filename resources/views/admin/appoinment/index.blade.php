@extends('admin.layout.master')


@section('content')


<div style="margin-left: 190px;" class="container" style="width: 100%;">
  <h2 style="margin-left: 408px; margin-top: 80px;">Find Latest Appoinment</h2>
  <hr>
  
  
    <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Advanced Table
              </header>

              <table class="table table-striped table-advance table-hover">
                <tbody>
                  <tr>
                    <th><i class="icon_profile"></i>Name</th>
                    <th><i class="icon_calendar"></i> Date</th>
                    <th><i class="icon_mail_alt"></i> Email</th>
                    <th><i class="icon_pin_alt"></i> Gender</th>
                    <th><i class="icon_mobile"></i> Specialization</th>
                    <th><i class="icon_mobile"></i> Phone</th>
                    <th><i class="icon_cogs"></i> Medication</th>
                     <th><i class="icon_cogs"></i>Action</th>
                  </tr>
                  <tr>
                    @if($appoinment)
                    @foreach($appoinment as $appoinment)
                    <td>{{ $appoinment->name }}</td>
                    <td>{{ $appoinment->date }}</td>
                    <td>{{ $appoinment->email }}</td>
                    <td>{{ $appoinment->gender }}</td>
                    <td>{{ $appoinment->specialization }}</td>
                    <td>{{ $appoinment->phone }}</td>
                    <td>{{ $appoinment->medication }}</td>
                    <td>
                      <div class="btn-group">
                        <a class="btn btn-primary" href="{{route('appoinment.create')}}"><i class="icon_plus_alt2"></i></a>
                        <a class="btn btn-success" href="#"><i class="icon_check_alt2"></i></a>
                          

                         <form id="delete-form-{{ $appoinment->id }}" action="{{ route('appoinment.destroy',$appoinment->id) }}" style="display: none;" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                    </form>
                    <button type="button" class="btn btn-danger " title="Delete" onclick="if(confirm('Are you sure? You want to delete this?')){
                        event.preventDefault();
                        document.getElementById('delete-form-{{ $appoinment->id }}').submit();
                    }else {
                        event.preventDefault();
                            }"><i class="icon_close_alt2"></i>
                    </button>

                      </div>
                    </td>
                  </tr>
                 
                      </div>
                    </td>
                  </tr>

                   @endforeach
                  @endif
                </tbody>
              </table>
            </section>
          </div>
        </div>
        </div>

@stop