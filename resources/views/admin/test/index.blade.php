@extends('admin.layout.master')


@section('content')


<div style="margin-left: 190px; width: 100%;" class="container" >
  <h2 style="margin-left: 408px; margin-top: 80px;">All Test Information</h2>
  <hr>
  
  
    <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                All Test List are here.....
              </header>

              <table class="table table-striped table-advance table-hover">
                <tbody>
                  <tr>
                    <th><i class="icon_profile"></i> Test Name</th>
                    <th><i class="icon_calendar"></i> Test Cost</th>
                     <th><i class="icon_calendar"></i> Action</th>
                  
                  </tr>
                  <tr>
                    @if($test)
                    @foreach($test as $test)
                    <td>{{ $test->tname }}</td>
                    <td>{{ $test->tcost }}</td>
                  
                    <td>
                      <div class="btn-group">
                        <a class="btn btn-primary" href="{{ route('test.edit',$test->id) }}"><i class="icon_plus_alt2"></i></a>

                        <a class="btn btn-success" href="#"><i class="icon_check_alt2"></i></a>

                        <form id="delete-form-{{ $test->id }}" action="{{ route('test.destroy',$test->id) }}" style="display: none;" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                    </form>
                    <button type="button" class="btn btn-danger btn" title="Delete" onclick="if(confirm('Are you sure? You want to delete this?')){
                        event.preventDefault();
                        document.getElementById('delete-form-{{ $test->id }}').submit();
                    }else {
                        event.preventDefault();
                            }"><i class="icon_close_alt2"></i>
                    </button>
                      </div>
                    </td>
                  </tr>
                 
                      </div>
                    </td>
                  </tr>
                   @endforeach
                  @endif
                </tbody>
              </table>
            </section>
          </div>
        </div>
        </div>


@stop