@extends('admin.layout.master')


@section('content')


<div style="margin-left: 190px;" class="container" style="width: 100%;">
	<h2 style="margin-left: 408px; margin-top: 80px;">Add Special Packages</h2>
	<hr>
	
  
 	 	<div class="row">
 	 		

 	 		<form method="post" action="{{route('package.store')}}" enctype="multipart/form-data">
        	
            {{csrf_field()}}
        	

	      <div class="col-md-6">

			<div class="form-group">
				  <label for="user">Name Of package:</label>
				  <input type="text" class="form-control" id="Name" name="npackage">
			</div> 

			<div class="form-group">
				  <label for="user">Twin Shared(cost):</label>
				  <input type="text" class="form-control" id="Name" name="tshared">
			</div> 

			<div class="form-group">
				  <label for="user">Single Standard(cost):</label>
				  <input type="text" class="form-control" id="Name" name="sstandard">
			</div> 

			<div class="form-group">
				  <label for="user">Single Delux(cost):</label>
				  <input type="text" class="form-control" id="Name" name="sdelux">
			</div> 

			<div class="form-group">
				  <label for="user">Image(Not mendatory):</label>
				  <input type="file"  id="usr" value="{{old('image')}}" name="image">
			</div>




			

			 <div class="btn-group">
         	<br>
			  <button type="submit" class="btn btn-success">Submit</button>
			  <button type="reset" class="btn btn-danger">Reset</button>
				  
		    </div>

		</div>


        
		<div class="col-md-6">


        <div class="form-group">
				  <label for="user">Suit(cost):</label>
				  <input type="text" class="form-control" id="Name" name="suit">
			</div> 

			<div class="form-group">
				  <label for="user">Duration of Hour(cost):</label>
				  <input type="text" class="form-control" id="Name" name="dhour">
			</div> 

			<div class="form-group">
				  <label for="user">Remarks(cost):</label>
				  <input type="text" class="form-control" id="Name" name="remarks">
			</div> 

			<div class="form-group">
				  <label for="user">Extra Charge:</label>
				  <input type="text" class="form-control" id="Name" name="echarge">
			</div> 
	
				 	

		
		
			


		      
		</div>
  		
  		
		</form>

		


		</div>	

</div>			    




@stop