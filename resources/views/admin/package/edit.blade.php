@extends('admin.layout.master')


@section('content')


<div style="margin-left: 190px;" class="container" style="width: 100%;">
	<h2 style="margin-left: 408px; margin-top: 80px;">Update Special Packages</h2>
	<hr>
	
  
 	 	<div class="row">
 	 		

 	 		<form method="post"  action="{{route('package.update', $package->id )}}" enctype="multipart/form-data">
        	{{csrf_field()}}
 			@method('put')
        	
            {{csrf_field()}}
        	

	      <div class="col-md-6">

			<div class="form-group">
				  <label for="user">Name Of package:</label>
				  <input type="text" class="form-control" id="Name" value="{{$package->npackage}}" name="npackage">
			</div> 

			<div class="form-group">
				  <label for="user">Twin Shared(cost):</label>
				  <input type="text" class="form-control" id="Name" value="{{$package->tshared}}" name="tshared">
			</div> 

			<div class="form-group">
				  <label for="user">Single Standard(cost):</label>
				  <input type="text" class="form-control" id="Name" value="{{$package->sstandard}}" name="sstandard">
			</div> 

			<div class="form-group">
				  <label for="user">Single Delux(cost):</label>
				  <input type="text" class="form-control" id="Name" value="{{$package->sdelux}}" name="sdelux">
			</div> 

			 <div class="form-group">
					<label for="user">Image:</label>
					<img style="width:80px;height:80px" src="{{ asset('image/'.$package->image) }}" alt="">
					 <input type="file"  id="usr" name="image">
			</div>

			 <div class="btn-group">
         	<br>
			  <button type="submit" class="btn btn-success">Submit</button>
			  <button type="reset" class="btn btn-danger">Reset</button>
				  
		    </div>

		</div>
        
		<div class="col-md-6">


        <div class="form-group">
				  <label for="user">Suit(cost):</label>
				  <input type="text" class="form-control" id="Name" value="{{$package->suit}}" name="suit">
			</div> 

			<div class="form-group">
				  <label for="user">Duration of Hour(cost):</label>
				  <input type="text" class="form-control" id="Name" value="{{$package->dhour}}" name="dhour">
			</div> 

			<div class="form-group">
				  <label for="user">Remarks(cost):</label>
				  <input type="text" class="form-control" id="Name" value="{{$package->remarks}}" name="remarks">
			</div> 

			<div class="form-group">
				  <label for="user">Extra Charge:</label>
				  <input type="text" class="form-control" id="Name" value="{{$package->echarge}}" name="echarge">
			</div> 		      
		</div>  		  		
		</form>
		</div>	

</div>			    




@stop