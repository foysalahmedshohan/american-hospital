@extends('admin.layout.master')


@section('content')


<div style="margin-left:210px; width:100%;" class="container">
  <h2 style="margin-left: 408px; margin-top: 80px;">All Packages</h2>
  <hr>
  
  
    <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Advanced Table
              </header>

              <table class="table table-striped table-advance table-hover">
                <tbody>
                  <tr>
                    <th><i class="icon_profile"></i> Name Of package</th>
                    <th><i class=""></i>Twin Shared(cost)</th>
                    <th><i class=""></i>Single Standard(cost)</th>
                    <th><i class="icon_pin_alt"></i>Single Delux(cost)</th>
                    <th><i class=""></i>Image</th>
                    <th><i class=""></i>Suit(cost)</th>
                    <th><i class=""></i>Duration of Hour(cost)</th>
                   
                    <th><i class=""></i>Remarks(cost)</th>
                    <th><i class=""></i>Extra Charge</th>

                    <th><i class="icon_cogs"></i> Action</th>
                  </tr>
                  <tr>
                     @if($package)
                    @foreach($package as $package)
                    <td>{{ $package->npackage }}</td> 
                    <td>{{ $package->tshared }}</td> 
                    <td>{{ $package->sstandard }}</td> 
                    <td>{{ $package->sdelux }}</td>  
                    <td class="hidden-phone"> <img  style="width: 65px; height: 65px;" src="{{ asset('image/'.$package->image) }}" /></td>                   
                    <td>{{ $package->suit }}</td> 
                    <td>{{ $package->dhour }}</td> 
                    <td>{{ $package->remarks }}</td> 
                    <td>{{ $package->echarge }}</td> 
                   
                    <td>
                      <div class="btn-group-vertical">
                          <a class="btn btn-primary" href="{{ route('package.edit',$package->id) }}"><i class="icon_plus_alt2"></i></a> 

                          <!--  <a class="btn btn-success" href="{{ route('package.create',$package->id) }}"><i class="icon_plus_alt2"></i></a>   -->                     

                       <form id="delete-form-{{ $package->id }}" action="{{ route('package.destroy',$package->id) }}" style="display: none;" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                    </form>
                    <button type="button" class="btn btn-danger btn" title="Delete" onclick="if(confirm('Are you sure? You want to delete this?')){
                        event.preventDefault();
                        document.getElementById('delete-form-{{ $package->id }}').submit();
                    }else {
                        event.preventDefault();
                            }"><i class="icon_close_alt2"></i>
                    </button>

                      </div>
                    </td>
                  </tr>
                 
                      </div>
                    </td>
                  </tr>
                   @endforeach
                  @endif
                </tbody>
              </table>
            </section>
          </div>
        </div>


@stop