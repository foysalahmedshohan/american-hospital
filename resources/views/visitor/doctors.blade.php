@extends('visitor.layout.master')


@section('content')

<div class="container">
	<h3 style="text-align: center; margin-top: 20px; color: red">Our Honorable Doctors</h3>
	<hr>
	<div class="row" style="min-height: 300px;">
		<div>

	 <table class="table table-striped table-bordered" ">
    <thead style="background-color:	#F8F8FF; padding:60px;">
    	<div style="height: 50px; width:100%; background-color: #0077c8;" class=" ">
    		
         <h4 style="color: white; text-align: center; padding-top:10px; ">	Doctor's list are here</h4>
    	</div>
      <tr>
        <th>Image</th>
        <th>Name</th>
        <th>Email</th>
        <th>Specalist</th>
        <th>Phone</th>
        <th>Designation</th>
        <th>Schedule</th>
      </tr>
    </thead>
    <tbody>
     
      <tr>
        @if($doctor)
          @foreach($doctor as $doctor)
          <td class="hidden-phone"> <img  style="width: 65px; height: 65px;" src="{{ asset('image/'.$doctor->image) }}" /></td>
          <td>{{ $doctor->name }}</td>                  
          <td>{{ $doctor->email }}</td>
          <td>{{ $doctor->specilities }}</td>
          <td>{{ $doctor->phone }}</td> 
          <td>{{ $doctor->designation }}</td> 
          <td>{{ $doctor->schedule }}</td>
      </tr>
       @endforeach
       @endif

     
    </tbody>
  </table>
			

		</div>
		<div>
			
		</div>
		
	</div>
</div>

@stop