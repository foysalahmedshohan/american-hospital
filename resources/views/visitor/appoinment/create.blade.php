@extends('visitor.layout.master')


@section('content')

<div class="appointmentform" id="appointmentform">
<div class="container">
	<div class="register-full">
		<div class="register-right">
			<div class="register-in">
		
				<h3>APPOINTMENT FORM</h3>
				<div class=" w3_abp">
				<div class="registerimg">
					<img style="height: 500px; margin-top: -50px" 	src="{{ asset ('visitor/images/app.jpg')}}" class="img-responsive1" alt="" />
				</div>
				</div>
				<div class=" register-form">

					<form method="post" action="{{route('appoinment.store')}}" enctype="multipart/form-data">
        	
            {{csrf_field()}}
						<div class="fields-grid">
							<div class="styled-input">
								<input type="text" name="name" required=""> 
								<label>Patients Name</label>
								<span></span>
							</div> 
							<div class="styled-input">
								<input type="email" name="email" required="">
								<label>Email</label>
								<span></span>
							</div>
							<div class="styled-input">
								<input type="text" name="phone" required=""> 
								<label>Phone Number</label>
								<span></span>
							</div> 
							<div class="styled-input">
								<h2>Sex :</h2>
								  <input type="radio" name="gender" value="male"> Male
								  <input type="radio" name="gender" value="female"> Female
							</div>
							<div class="styled-input">
								<select id="category1" name="specialization" required="">

								<option name="specialization" value="">Specialization*</option>

								<option name="specialization" value="specialization">Cardiology</option>

								<option name="specialization" value="specialization">Heart Surgery</option>

								<option name="specialization" value="specialization">Skin Care</option>

								<option name="specialization" value="specialization">Body Check-up</option>

								<option name="specialization" value="specialization">Numerology</option>
								<option name="specialization" value="specialization">Diagnosis</option>
								<option name="specialization" value="specialization">Others</option>
								</select>
								<span></span>
							</div>
						
							<div class="styled-input">
							<input class="date" id="datepicker" name="date" type="text" value="MM/DD/YYYY" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'MM/DD/YYYY';}"  required="">
							</div>	
								<div class="content-wthree2">
									<div class="grid-w3layouts1">
										<div class="w3-agile1">
											<label class="rating">Taking any medications currently?<span>*</span></label>
											<ul>
												<li>
													<input type="radio" id="a-option" name="medication">
													<label for="a-option" name="medication">Yes </label>
													<div class="check"></div>
												</li>
												<li>
													<input type="radio" id="b-option" name="medication">
													<label for="b-option" name="medication">No</label>
													<div class="check"><div class="inside"></div></div>
												</li>
											</ul>
												<div class="clear"></div>
										</div>
									</div>
									<div class="clear"></div>
								</div>
								
								<input type="submit" name="submit" value="Book Appointment">
							
						</div>
						
					</form>
						
				</div>
				<div class="clearfix"></div>
			 </div>
		</div>
		</div>
	</div>
	</div>

@stop