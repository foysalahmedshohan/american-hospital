@extends('visitor.layout.master')


@section('content')

<div class="container">
  <h3 style="text-align: center; margin-top: 20px; color: red">All Speciall Equipment</h3>
  <hr>
  <div class="row col-md-12" style="min-height: 300px;">
    <div class="col-md-6" >

   <table class="table table-striped table-bordered">
    <thead style="background-color: #F8F8FF;">
      <div style="height: 50px; width:100%; background-color: #0077c8;" class=" ">
        
         <h4 style="color: white; text-align: center; padding-top:10px;"> We have lots of speciall equipment</h4>
      </div>
      <tr>
        <th>Serial No.</th>
        <th>Name</th>
        
       
      </tr>
    </thead>
    <tbody>
     
      <tr>
        <td>1</td>
        <td>Microbiological Incubator</td>
     
      </tr>
      </tr>

       <tr>
        <td>2</td>
        <td>Warming Incubator oven with 3 doors</td>
        
        
      </tr>
      </tr>
      <tr>
        <td>3</td>
        <td>Syringe & Infusion Pump</td>
       
      </tr>
    
     <tr>
        <td>4</td>
        <td>ETT Machine with Treadmill</td>
       
      </tr>
    
     <tr>
        <td>5</td>
        <td>4D Ultrasound Machine</td>
       
      </tr>
    
     <tr>
        <td>6</td>
        <td>CT Scan Machine VCT-500</td>
       
      </tr>
    
    
     <tr>
        <td>7</td>
        <td>OT Table, Amsco 3085 SP</td>
       
      </tr>
    
     <tr>
        <td>8</td>
        <td>Syringe & Infusion Pump</td>
       
      </tr>
    
     <tr>
        <td>9</td>
        <td>Radiotherapy Machine( Linac)</td>
       
      </tr>
    
     <tr>
        <td>10</td>
        <td>Laparoscopy System</td>
       
      </tr>
    
     <tr>
        <td>11</td>
        <td>Anesthesia Machine with Ventilator
</td>
       
      </tr>

       <tr>
        <td>12</td>
        <td>OT Table, Amsco 3085 SP</td>
       
      </tr>

       <tr>
        <td>13</td>
        <td>Pneumatic Drill Machine</td>
       
      </tr>

       <tr>
        <td>14</td>
        <td>Microscope,S88(OPMIvario)</td>
       
      </tr>

       <tr>
        <td>15</td>
        <td>Arthro Pump & Arthroscope</td>
       
      </tr>

       <tr>
        <td>16</td>
        <td>Diathermy Machine</td>
       
      </tr>




       <tr>
        <td>17</td>
        <td>Echo Machine</td>
       
      </tr>


       <tr>
        <td>18</td>
        <td>Mobile X-Ray</td>
       
      </tr>


       <tr>
        <td>19</td>
        <td>Patient Monitor</td>
       
      </tr>


       <tr>
        <td>20</td>
        <td>Defibrillator Machine</td>
       
      </tr>
      <tr>
        <td>21</td>
        <td>Eco Machine</td>
       
      </tr>
   
    
     
      </tr>
    </tbody>
  </table>
    </div>
    

  
  
  <div class="col-md-3">
  <img style="height: 276px;"   src="{{ asset ('visitor/images/5.jpg')}}" class="img-responsive1" alt="" />
  <br> <br>
  <img style="height: 276px;"   src="{{ asset ('visitor/images/19.jpg')}}" class="img-responsive1" alt="" />

    <br> <br>
  <img style="height: 276px;"   src="{{ asset ('visitor/images/3.jpg')}}" class="img-responsive1" alt="" />

      
  </div>

  <div class="col-md-3">
  <img style="height: 276px;"   src="{{ asset ('visitor/images/28.jpg')}}" class="img-responsive1" alt="" />

   <br> <br>
  <img style="height: 276px;"   src="{{ asset ('visitor/images/23.jpg')}}" class="img-responsive1" alt="" />

    <br> <br>
  <img style="height: 276px;"   src="{{ asset ('visitor/images/24.jpg')}}" class="img-responsive1" alt="" />
      
  </div>

</div>
</div>

@stop