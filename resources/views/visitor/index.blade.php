@extends('visitor.layout.master')


@section('content')
<!-- /Header-->
<!-- banner-->
<div class="agile_banner">
<div class="s1">
			<h3>MAKING</h3>
			<h4>YOUR LIFE EASY AND <span class="chng">HAPPY</span></h4>
			<p> Lorem Ipsum is simply dummy text of the typesetting industry Lorem Ipsum is simply dummy text of the typesetting industry</p>
			<div class="w3-button">
				<div class="w3ls-button">
					<a href="#SPECIALISTS" class="scroll">Our Specialist</a>
				</div>
				<div class="w3l-button">
					<a href="#departments" class="scroll">Portfolio</a>
				</div>
				<div class="clearfix"> </div>
			</div>

		</div>


</div>
<!--/banner-->

	<div class="services" id="services">
		<div class="container">
						<div class="wthree_head_section" style="margin-top:-30px; ">
				<h3 class="w3l_header w3_agileits_header two">OUR <span>SERVICES</span></h3>

				
			</div>

			<div class="agile_wthree_inner_grids" style="margin-top:-40px; ">
				<div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
					<div class="agileinfo_banner_bottom_pos">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-md-2 col-sm-2 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-map-o" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-md-10 col-sm-10 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">CARDIOLOGY</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
					<div class="agileinfo_banner_bottom_pos1">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-md-2 col-sm-2 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-rocket" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-md-10 col-sm-10 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">RADIOLOGY</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 agileits_banner_bottom_left">
					<div class="agileinfo_banner_bottom_pos2">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-md-2 col-sm-2 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-paint-brush" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-md-10 col-sm-10 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">DENTAL</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 agileits_banner_bottom_left two">
					<div class="agileinfo_banner_bottom_pos3">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-md-2 col-sm-2 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-pencil" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-md-10 col-sm-10 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">PEDIATRICS</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 agileits_banner_bottom_left  two">
					<div class="agileinfo_banner_bottom_pos4">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-md-2 col-sm-2 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-fire" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-md-10 col-sm-10 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">DERMITOLOGY</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 agileits_banner_bottom_left  two">
					<div class="agileinfo_banner_bottom_pos5">
						<div class="w3_agileits_banner_bottom_pos_grid">
							<div class="col-md-2 col-sm-2 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
									<i class="fa fa-video-camera" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-md-10 col-sm-10 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits">NEUROLOGY</h4>
								<p>Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat.</p>
								
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
<div class="clearfix"></div>
			</div>
		</div>
	</div>



<!-- About-->
<div class="about" id="about">
<div class="container">


          <div style="margin-top:-100px; " class="row team-data" id="SPECIALISTS">
		  <h3>OUR SPECIALISTS</h3>
            <div class="col-md-3 col-sm-3 col-xs-6 w3_src">
              <div class="team-profile">
			  <div class="member-social" align="center">
                  <a href=""><i class="fa fa-facebook"></i></a>
                  <a href=""><i class="fa fa-twitter"></i></a>
                  <a href=""><i class="fa fa-google"></i></a>
                </div>
                <div class="member-img" align="center">
                  <img src="{{ asset ('visitor/images/ab1.jpg')}}" alt="">
                </div>
              
                <div class="member-social1" align="center">
                  <h4>Michel</h4>
				  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                </div>
            </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 ">
              <div class="team-profile">
			  <div class="member-social" align="center">
                  <a href=""><i class="fa fa-facebook"></i></a>
                  <a href=""><i class="fa fa-twitter"></i></a>
                  <a href=""><i class="fa fa-google"></i></a>
                </div>
                <div class="member-img" align="center">
                  <img src="{{ asset ('visitor/images/ab2.jpg')}}" alt="">
                </div>
                
              <div class="member-social1" align="center">
                  <h4>Andy</h4>
				  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                </div>
              </div>
            </div>  <!-- member 1 ends here  -->
            <div class="col-md-3 col-sm-3 col-xs-6">
              <div class="team-profile">
			  <div class="member-social" align="center">
                  <a href=""><i class="fa fa-facebook"></i></a>
                  <a href=""><i class="fa fa-twitter"></i></a>
                  <a href=""><i class="fa fa-google"></i></a>
                </div>
                <div class="member-img" align="center">
                  <img src="{{ asset ('visitor/images/ab3.jpg')}}" alt="">
                </div>
                
                <div class="member-social1" align="center">
                  <h4>Shawn</h4>
				  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                </div>
              </div>
            </div> <!-- member 2 ends here  -->
            <div class="col-md-3 col-sm-3 col-xs-6">
              <div class="team-profile">
			  <div class="member-social" align="center">
                  <a href=""><i class="fa fa-facebook"></i></a>
                  <a href=""><i class="fa fa-twitter"></i></a>
                  <a href=""><i class="fa fa-google"></i></a>
                </div>
                <div class="member-img" align="center">
                  <img src="{{ asset ('visitor/images/ab4.jpg')}}" alt="">
                </div>
               
                <div class="member-social1" align="center">
                  <h4>Amy</h4>
				  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                </div>
              </div>
			  
			  </div>
			  <div class="clearfix"></div>
			<!-- member 3 ends here  -->
          </div>
		  
      
   </div>
</div>
<!-- /About-->

<!-- services -->

	<!-- //services -->
	
<!-- testimonals section -->
<div class="testimonials" id="testimonials">
		<div class="container">
			
				<h3>TESTIMONIALS</h3>
		
			<div class="agileits-feedback-grids">
				<div id="owl-demo" class="owl-carousel owl-theme">
					<div class="item">
						<div class="test-review">
										<div class="w3_ic">
										<i class="fa fa-quote-left" aria-hidden="true"></i>
										</div>
										<p>Clinical cardiac electrophysiology is a branch of the medical specialty of cardiology and is concerned with the   </p>
											<div class="img-agile">
												<img src="{{ asset ('visitor/images/t1.jpg')}}" class="img-responsive" alt=""/>
											</div>
											  <h5>Rita</h5>
					                	    </div>
					</div>
					<div class="item">
					<div class="test-review">
									<div class="w3_ic">
										<i class="fa fa-quote-left" aria-hidden="true"></i>
										</div>
										 <p>This article, the second in a five-part series on nuclear medicine imaging, explains how bone scintigraphy works   </p>
						                	<div class="img-agile">
											<img src="{{ asset ('visitor/images/t2.jpg')}}" class="img-responsive" alt=""/>
											</div>
											<h5>Williams Allen</h5>
					                	</div>
					</div>
					<div class="item">
						<div class="test-review">
										<div class="w3_ic">
										<i class="fa fa-quote-left" aria-hidden="true"></i>
										</div>
										     <p>This article discusses the principles of decontamination and how to standardise the cleaning of shared</p>
						                	<div class="img-agile">
											<img src="{{ asset ('visitor/images/t3.jpg')}}" class="img-responsive" alt=""/>
											</div>
											<h5>Laila</h5>
					                	</div>
					</div>
					<div class="item">
						<div class="test-review">
										<div class="w3_ic">
										<i class="fa fa-quote-left" aria-hidden="true"></i>
										</div> 
										     <p>Polite sadipscing elitr, sed diam is nonumy is eirmod tempor invidunt ut labore et dolore magna aliquyam erat,  </p>
						                	<div class="img-agile">
											<img src="{{ asset ('visitor/images/t4.jpg')}}" class="img-responsive" alt=""/>
											</div>
											<h5>Stella</h5>
					                	</div>
					</div>
					<div class="item">
						<div class="test-review">
										<div class="w3_ic">
										<i class="fa fa-quote-left" aria-hidden="true"></i>
										</div>
										     <p>Polite sadipscing elitr, sed diam is nonumy is eirmod tempor invidunt ut labore et dolore magna aliquyam erat,  </p>
						                	<div class="img-agile">
											<img src="{{ asset ('visitor/images/t5.jpg')}}" class="img-responsive" alt=""/>
											</div>
											<h5>Rick</h5>
					                	</div>
					</div>
					<div class="item">
						<div class="test-review">
						<div class="w3_ic">
										<i class="fa fa-quote-left" aria-hidden="true"></i>
										</div>
										     <p>Polite sadipscing elitr, sed diam is nonumy is eirmod tempor invidunt ut labore et dolore magna aliquyam erat,  </p>
						                	<div class="img-agile">
											<img src="{{ asset ('visitor/images/t6.jpg')}}" class="img-responsive" alt=""/>
											</div>
											<h5>Richard</h5>
					                	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- /testimonals section -->




<!-- Tabs-->

<div class="departments" id="departments">
<h3>OUR DEPARTMENTS</h3>
<div class="sap_tabs">
<div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
<ul class="resp-tabs-list">
<li class="resp-tab-item" ><span>Cardiology</span></li>
<li class="resp-tab-item" ><label></label><span>Neurology</span></li>
<li class="resp-tab-item" ><label></label><span>Dental</span></li>
<li class="resp-tab-item" ><label></label><span>Dermitology</span></li>
<li class="resp-tab-item" ><label></label><span>Radiology</span></li>
</ul>
<div class="container">

<div class="tab-1 resp-tab-content" >
<div class="col-md-6 col-sm-6 w3_tm">
<h4>Cardiology</h4>
<p>Clinical cardiac electrophysiology is a branch of the medical specialty of cardiology and is concerned with the study and treatment of rhythm disorders of the heart. Cardiologists with expertise in this area are usually referred to as electrophysiologists. Electrophysiologists are trained in the mechanism, function, and performance of the electrical activities of the heart. Electrophysiologists work closely with other cardiologists and cardiac surgeons to assist or guide therapy for heart rhythm disturbances (arrhythmias). They are trained to perform interventional and surgical procedures to treat cardiac arrhythmia.
</p>
</div>
<div class="col-md-6 col-sm-6 w3_im">
<img src="{{ asset ('visitor/images/d1.jpg')}}" class="img-responsive">
</div>
<div class="clearfix"></div>	
</div>
<div class="tab-1 resp-tab-content" >
<div class="col-md-6 col-sm-6 w3_tm">
<h4>Neurology</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>
<div class="col-md-6 col-sm-6 w3_im">
<img src="{{ asset ('visitor/images/d2.jpg')}}" class="img-responsive">
</div>
<div class="clearfix"></div>	
</div>
<div class="tab-1 resp-tab-content" >
<div class="col-md-6 col-sm-6 w3_tm">
<h4>Dental</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>
<div class="col-md-6 col-sm-6 w3_im">
<img src="{{ asset ('visitor/images/d3.jpg')}}" class="img-responsive">
</div>
<div class="clearfix"></div>
</div>
<div class="tab-1 resp-tab-content" >
<div class="col-md-6 col-sm-6 w3_tm">
<h4>Dermitology</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>
<div class="col-md-6 col-sm-6 w3_im">
<img src="{{ asset ('visitor/images/d4.jpg')}}" class="img-responsive">
</div>
<div class="clearfix"></div>
</div>
<div class="tab-1 resp-tab-content" >
<div class="col-md-6 col-sm-6 w3_tm">
<h4>Radiology</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>
<div class="col-md-6 col-sm-6 w3_im">
<img src="{{ asset ('visitor/images/d5.jpg')}}" class="img-responsive">
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
</div>
</div>
<!--/Tabs-->

<!-- Appointment Form -->


<!-- //Appointment Form -->

<!-- gallery -->
   <div class="gallery" id="gallery">
         <div class="container">
            <h3>OUR LABORATORIES</h3>
          
            <div class="about-bottom  w3ls-team-info">
                <div class="col-md-12">
                    <div id="Carousel" class="carousel slide">
                        <!-- Carousel items -->
                        <div class="carousel-inner">

                            <div class="item active">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-6 img-gallery-w3l">
                                        <a href="{{ asset ('visitor/images/g6.jpg')}}" class="thumbnail cm-overlay">
                                            <img src="{{ asset ('visitor/images/g6.jpg')}}" class="img-responsive" alt="Image" style="max-width:100%;">
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6 img-gallery-w3l">
                                        <a href="{{ asset ('visitor/images/g3.jpg')}}" class="thumbnail cm-overlay">
                                            <img src="{{ asset ('visitor/images/g3.jpg')}}" class="img-responsive" alt="Image" style="max-width:100%;">
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6 img-gallery-w3l">
                                        <a href="{{ asset ('visitor/images/g7.jpg')}}" class="thumbnail cm-overlay">
                                            <img src="{{ asset ('visitor/images/g7.jpg')}}" class="img-responsive" alt="Image" style="max-width:100%;">
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6 img-gallery-w3l">
                                        <a href="{{ asset ('visitor/images/g6.jpg')}}" class="thumbnail cm-overlay">
                                            <img src="{{ asset ('visitor/images/g6.jpg')}}" class="img-responsive" alt="Image" style="max-width:100%;">
                                        </a>
                                    </div>
                                </div>
                                <!--.row-->
                            </div>
                            
                            
						
							
							<div class="item active">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-6 img-gallery-w3l">
                                        <a href="images/g1.jpg" class="thumbnail cm-overlay">
                                            <img src="{{ asset ('visitor/images/g1.jpg')}}" class="img-responsive" alt="Image" style="max-width:100%;">
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6 img-gallery-w3l">
                                        <a href="images/g2.jpg" class="thumbnail cm-overlay">
                                            <img src="{{ asset ('visitor/images/g2.jpg')}}" class="img-responsive" alt="Image" style="max-width:100%;">
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6 img-gallery-w3l">
                                        <a href="images/g3.jpg" class="thumbnail cm-overlay">
                                            <img src="{{ asset ('visitor/images/g3.jpg')}}" class="img-responsive" alt="Image" style="max-width:100%;">
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6 img-gallery-w3l">
                                        <a href="images/g4.jpg" class="thumbnail cm-overlay">
                                            <img src="{{ asset ('visitor/images/g4.jpg')}}" class="img-responsive" alt="Image" style="max-width:100%;">
                                        </a>
                                    </div>
                                </div>
                                <!--.row-->
                            </div>
							
							
						
                        </div>
					
                     

                </div>
            </div>
			
        </div>
    	
	</div>
	</div>
	
    <!-- //gallery -->


	<!-- footer -->
@stop