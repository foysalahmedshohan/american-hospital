<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title> Saydur Memorial Hospital</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Curative a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<link href=" {{ asset ('visitor/css/bootstrap.min.css')}} " rel="stylesheet" type="text/css" media="all">
<link href=" {{ asset ('visitor/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href=" {{ asset ('visitor/css/owl.carousel.css')}}" type="text/css" media="all">
<link href="{{ asset ('visitor/css/owl.theme.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset ('visitor/css/jquery-ui.css')}}" type="text/css" media="all" />
<link type="text/css" rel="stylesheet" href="{{ asset ('visitor/css/cm-overlay.css')}}" />
<link href="{{ asset ('visitor/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Abel" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>
<!-- Header -->
	<div class="header-top">
		<div class="container">
			<div class="bottom_header_left">
			<div class="w3_fr">
				<a href="#">Email: saydur@gmail.com</a>
				<a href="#contact" class="scroll">+8801666666666</a>
				
				</div>
			</div>
			<div class="bottom_header_right">
				<div class="bottom-social-icons">
					<a class="facebook" href="#">
						<span class="fa fa-facebook"></span>
					</a>
					<a class="twitter" href="#">
						<span class="fa fa-twitter"></span>
					</a>
					<a class="pinterest" href="#">
						<span class="fa fa-pinterest-p"></span>
					</a>
					<a class="linkedin" href="#">
						<span class="fa fa-linkedin"></span>
					</a>
				</div>
				<div class="head-search">
				<form action="#" method="post">
					
				</form>
			</div>


				
				
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="header">
		<div class="content white">
			<nav class="navbar navbar-default">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html">
							<h1>
								Saydur
								<label>Memorial Hospital </label>
							</h1>
						</a>
					</div>
					<!--/.navbar-header-->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<nav class="link-effect-2" id="link-effect-2">
							<ul class="nav navbar-nav navbar-right">
								<li>
									<a href="{{route('visitor')}}">Home</a>
								</li>
								<li class="dropdown">
								  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Doctor<span class="caret"></span></a>
								  <ul class="dropdown-menu">
								   <li>
									<a href="{{route('doctor.visitor')}}" class="">Our Doctors</a>
								</li>

								<li>
									<a href="{{route('appoinment.create')}}" class="">Appoinment</a>
								</li>
								
							  </ul>
							</li>
								
								<li>
									<a href="{{route('test.visitor')}}" class="">Medical Test</a>
								</li>

                                <li>
									<a href="{{route('seat.visitor')}}"  class="">Seat</a>
								</li>

								<li>
									<a href="{{route('package.visitor')}}" class="">Package</a>
								</li>
								<li>
									<a href="{{route('equipment.visitor')}}" class="">Equipment</a>
								</li>
								
								
								
								<li class="dropdown">
								  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Others<span class="caret"></span></a>
								  <ul class="dropdown-menu">
								  

								<li>
									<a href="{{route('video.visitor')}}" class="">Video</a>
								</li>

								<li>
									<a href="{{route('gallery.visitor')}}" class="">gallery</a>
								</li>								

								  <li>
									<a href="#contact" class="">Contact Us</a>
								</li>
								<li>
									<a href="#contact" class="">About Us</a>
								</li>

								<li>
									<a href="{{ url('login') }}" class="">Login</a>
								</li>
								
								
							  </ul>
							</li>
							</ul>
						</nav>
					</div>
					<!--/.navbar-collapse-->
					<!--/.navbar-->
				</div>
			</nav>
		</div>
	</div>
	<div class="headerdown">
	<div class="container">
	<div class="col-md-4 col-sm-4 w3_hl">
	<div class="w3l_l">
	<i class="fa fa-phone-square" aria-hidden="true"></i>
	</div>
	<div class="w3l_r">
	<h3>call Today 12345678</h3>
	<h5>Ask for Doctor</h5>
	</div>
	
	</div>
	
	<div class="col-md-4 col-sm-4 w3_hc">
	<div class="w3l_cl">
	<i class="fa fa-clock-o" aria-hidden="true"></i>
	</div>
	<div class="w3l_cr">
	<h3>Open Hours</h3>
	<h5>Mon-sun(08:00-23:00 hrs)</h5>
	</div>
	
	</div>
	
	<div class="col-md-4 col-sm-4 w3_hr">
	<div class="w3l_rl">
	<i class="fa fa-book" aria-hidden="true"></i>
	</div>
	<div class="w3l_rr">
	<a href="#appointmentform" class="scroll">
	<h3>For an Appointment</h3>
	<h5>Book Now</h5>
	</a>
	</div>
	
	</div>
	<div class="clearfix"></div>
	</div>
	</div>
<!-- /Header-->
<!-- banner-->

<!-- /testimonals section -->

   <section id="main-content">
      <section class="wrapper">
       @yield('content')
      </section>
    </section>
	
    <!-- //gallery -->


	<!-- footer -->
	<div class="contact" id="contact">
	<div class="container">
		<div class="f-bg-w3l">
		<h3>CONTACT US</h3>
				<div class="col-md-4  w3layouts_footer_grid1">
				<div class="form-bg-w3ls">
					<h4 class="subhead-agileits white-w3ls">Get in Touch</h4>
					<form action="#" method="post">
						<input type="text" name="Name" placeholder="Name" required="">
						<input type="email" name="Email" placeholder="Email" required="">
						<textarea name="Message" placeholder="Message" required=""></textarea>
						<div class="w3_cs">
						<input type="submit" value="SEND" class="button-w3layouts hvr-rectangle-out">
						</div>
					</form>
				</div>	
				</div>
				<div class="col-md-4  w3layouts_footer_grid">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387193.30591910525!2d-74.25986432970718!3d40.697149422113014!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1515126770442" width="300" height="320"></iframe>
				</div>
				<div class="col-md-4  w3layouts_footer_grid">
					<h4>Address</h4>
					    <ul class="con_inner_text">
							<li><span class="fa fa-map-marker" aria-hidden="true"></span>1234k Avenue, 4th block, <label> New York City.</label></li>
							<li><span class="fa fa-envelope-o" aria-hidden="true"></span> <a href="mailto:info@example.com">info@example.com</a></li>
							<li><span class="fa fa-phone" aria-hidden="true"></span> +1234 567 567</li>
							<li><span class="fa fa-phone" aria-hidden="true"></span> +1234 567 567</li>
						</ul>

					<ul class="social_agileinfo">
						<li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#" class="w3_google"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>


				<div class="clearfix"> </div>
					<p class="copyright">© 2018 Daffodil. All Rights Reserved | Design by <a href="" target="_blank">w3layouts</a></p>
			</div>
			</div>
	</div>
	<!-- //footer -->
	
<script src="{{ asset ('visitor/js/jquery.min.js')}}"></script>
<script src="{{ asset ('visitor/js/bootstrap.min.js')}}"></script>
<script  src="{{ asset ('visitor/js/move-top.js')}}"></script>
<script  src="{{ asset ('visitor/js/easing.js')}}"></script>
<script  src="{{ asset ('visitor/js/SmoothScroll.min.js')}}"></script>	
	<!-- for testimonials slider-js-file-->
			<script src="{{ asset ('visitor/js/owl.carousel.js')}}"></script>
	<!-- //for testimonials slider-js-file-->
		<script>
		$(document).ready(function() { 
		$("#owl-demo").owlCarousel({
 
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			autoPlay:true,
			items : 3,
			itemsDesktop : [640,5],
			itemsDesktopSmall : [414,4]
		});
		}); 
</script>
<!-- for testimonials slider-js-script-->

 <!--script-->
<script src="{{ asset ('visitor/js/easyResponsiveTabs.js')}}" type="text/javascript"></script>
		    <script type="text/javascript">
			    $(document).ready(function () {
			        $('#horizontalTab').easyResponsiveTabs({
			            type: 'default', //Types: default, vertical, accordion           
			            width: 'auto', //auto or any width like 600px
			            fit: true   // 100% fit in a container
			        });
			    });
				
</script>
<!--script-->
<!-- Calendar -->
<script src="{{ asset ('visitor/js/jquery-ui.js')}}"></script>
	<script>
		$(function() {
		$( "#datepicker,#datepicker1" ).datepicker();
		});
	</script>
<!-- //Calendar -->
<!-- /gallery -->
    <script src="{{ asset ('visitor/js/jquery.tools.min.js')}}"></script>
    <script src="{{ asset ('visitor/js/jquery.mobile.custom.min.js')}}"></script>
    <script src="{{ asset ('visitor/js/jquery.cm-overlay.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.cm-overlay').cmOverlay();
        });
    </script>
    <!-- //gallery -->
<!-- start-smoth-scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- scrolling script -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
<!-- //scrolling script -->
<!--//start-smoth-scrolling -->

</body>
</html>