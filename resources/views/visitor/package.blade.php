@extends('visitor.layout.master')


@section('content')

<div class="container">
	<h3 style="text-align: center; margin-top: 20px; color: red">Special package are here</h3>

	<hr>
	<div class="row" style="min-height: 300px;">
		<div>

	 <table class="table table-striped table-bordered">
    <thead style="background-color:	#F8F8FF;">
    	<div style="height: 50px; width:100%; background-color: #0077c8;" class=" ">
    		
         <h4 style="color: white; text-align: center; padding-top:10px;">	We Always Try to gives best package to our customer..... </h4>
    	</div>
      <tr>
        <th>Serial No.</th>
        <th>Name Of package</th>
        <th>Twin Shared</th>
        <th>Single Standard</th>
        <th>Single Delux</th>
        <th>Image</th>
        <th>Suit</th>
        <th>Duration of Hour</th>
        <th>Remarks</th>
         <th>Extra Charge</th>
      </tr>
    </thead>
    <tbody>
     
      <tr>
         @if($package)
          @foreach($package as $package)
          <td>1</td> 
       <td>{{ $package->npackage }}</td> 
          <td>{{ $package->tshared }}</td> 
          <td>{{ $package->sstandard }}</td> 
          <td>{{ $package->sdelux }}</td>  
          <td class="hidden-phone"> <img  style="width: 65px; height: 65px;" src="{{ asset('image/'.$package->image) }}" /></td>                   
          <td>{{ $package->suit }}</td> 
          <td>{{ $package->dhour }}</td> 
          <td>{{ $package->remarks }}</td> 
          <td>{{ $package->echarge }}</td>         
      </tr>
       @endforeach
      @endif
      
   
    </tbody>
  </table>
			

		</div>
		<div>
			
		</div>
		
	</div>
</div>

@stop