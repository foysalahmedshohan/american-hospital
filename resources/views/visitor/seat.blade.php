@extends('visitor.layout.master')


@section('content')

<div class="container">
	<h3 style="text-align: center; margin-top: 20px; color: red">Room Availability & Cost</h3>
	<hr>
 <div class="row col-md-12" style="min-height: 300px;">
    <div class="col-md-6" >

	 <table class="table table-striped table-bordered">
    <thead style="background-color:	#F8F8FF;">
    	<div style="height: 50px; width:100%; background-color: #0077c8;" class=" ">
    		
         <h4 style="color: white; text-align: center; padding-top:10px;">	Seat Rent and Availibility are here</h4>
    	</div>
      <tr>
        <th>Serial No.</th>
        <th>Room Type</th>
        <th>Total Seat</th>
        <th>Available Seat</th>
        <th>Charge</th>
  
      </tr>
    </thead>
    <tbody>
     <!-- {{$i=0}} -->
      <tr>
          @if($seat)
          @foreach($seat as $seat)
          <td>{{$i++ }}</td>
          <td>{{ $seat->stype }}</td> 
          <td>{{ $seat->tseat }}</td>
          <td>{{ $seat->aseat }}</td>
          <td>{{ $seat->charge }}</td>           
      </tr>
       @endforeach
      @endif
 
      </tr>
    </tbody>
  </table>
			

  </div>

	   <div class="col-md-3">
      <img style="height: 276px;"   src="{{ asset ('visitor/images/2.jpg')}}" class="img-responsive1" alt="" />
      <br> <br>
      <img style="height: 276px;"   src="{{ asset ('visitor/images/29.jpg')}}" class="img-responsive1" alt="" />

      
    </div>

      <div class="col-md-3">
      <img style="height: 276px;"   src="{{ asset ('visitor/images/21.jpg')}}" class="img-responsive1" alt="" />
      <br> <br>
      <img style="height: 276px;"   src="{{ asset ('visitor/images/32.jpg')}}" class="img-responsive1" alt="" />
 
    </div>
	
	</div>

</div>

@stop