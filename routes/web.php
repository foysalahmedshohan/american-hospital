<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group(['middleware'=>'auth'], function(){


Route::resource('appoinment', 'visitor\appoinmentController');
Route::resource('doctor', 'admin\DoctorController');
Route::resource('seat', 'admin\SeatController');
Route::resource('test', 'admin\TestController');
Route::resource('package', 'admin\PackageController');
Route::resource('video', 'admin\VideoController');
Route::resource('gallery', 'admin\GalleryController');
Route::resource('Equipment', 'admin\EquipmentController');

Route::get('/admin/index','admin\OthersController@index')->name('admin');

});



Route::get('/testall','admin\TestController@visitor')->name('test.visitor');
Route::get('/packageall','admin\PackageController@visitor')->name('package.visitor');

Route::get('/seatall','admin\SeatController@visitor')->name('seat.visitor');
Route::get('/doctorall','admin\DoctorController@visitor')->name('doctor.visitor');

Route::get('/equipmentall','admin\EquipmentController@visitor')->name('equipment.visitor');

Route::get('/videoall','admin\VideoController@visitor')->name('video.visitor');
Route::get('/galleryall','admin\GalleryController@visitor')->name('gallery.visitor');
Route::get('/','admin\GalleryController@visitorhome')->name('visitor');


//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
