<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable=['npackage','tshared','sstandard','sdelux','image','suit','dhour','remarks','echarge'];
}
