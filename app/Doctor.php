<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
     protected $fillable=['name','email','address','specilities','designation','bd','degree','phone','schedule','image'];
}
