<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Seat;
use Carbon\Carbon;

class SeatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $seat = Seat::all();
         return view('admin.room.index', compact('seat'));
    }


       public function visitor()
    {
        $seat = Seat::all();
         return view('visitor.seat', compact('seat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view ('admin.room.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $seat = new Seat();

        $image = $request->file('image');
        $slug = str_slug($request->stype);

        if (isset($image)) {

            $currentDate = Carbon::now()->toDateString();
            
            $image_name = $slug.'-'.$currentDate .uniqid(). '.' .$image->getClientOriginalExtension();
            if (!file_exists('image')) {
                mkdir('image', 0777, true);
            }
            $image->move('image',$image_name);
        }

        $seat->stype =$request->stype;
       // $doctor->user_id = Auth::guard('dashboard')->id();
        $seat->tseat =$request->tseat;
        $seat->image =$request->image;
        $seat->aseat =$request->aseat;
        $seat->charge =$request->charge;       
        $seat->save();
        
        // Toastr::success('Doctor Successfully Added','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         {
        //$ambulance=Ambulance::where('id',auth()->id())->first();
            $seat=seat::where('id',$id)->first();

            // dd($ambulance);

        if ($seat==null){

            //return 'Post Not found';
            return abort(404);
        }

        // $post=Post::where('author_id',$d)->orWhere('id',$d)->first();
        return view('admin.room.edit')->with('seat',$seat);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $seat = seat::findOrFail($id);

        $image = $request->file('image');
        $slug = str_slug($request->stype);

        if (isset($image)) {

            $currentDate = Carbon::now()->toDateString();
            
            $image_name = $slug.'-'.$currentDate .uniqid(). '.' .$image->getClientOriginalExtension();
            
            unlink('image/'.$seat->image);
            $image->move('image/',$image_name);

        }else{
            $image_name = $seat->image;
        }
        
         $seat->stype =$request->stype;
       // $doctor->user_id = Auth::guard('dashboard')->id();
        $seat->tseat =$request->tseat;
        $seat->image =$request->image;
        $seat->aseat =$request->aseat;
        $seat->charge =$request->charge;       
        $seat->save();
       
        // Toastr::success('Doctor Successfully Added','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back();  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         {
        $seat = seat::find($id);

        if (file_exists('image/'.$seat->image)) {

            //unlink('image/'.$doctor->image);

        }

        $seat->delete();

        // Toastr::success('Ambulance Successfully Deleted','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back(); 
    }
    }
}
