<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Test;
use Carbon\Carbon;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $test = Test::all();
         return view('admin.test.index', compact('test'));
    }


     public function visitor()
    {
        $test = Test::all();
         return view('visitor.test', compact('test'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.test.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        //dd();
         $test = new Test();      
       
        $test->tname =$request->tname;
        $test->tcost =$request->tcost;       
        $test->save();
        
        // Toastr::success('Doctor Successfully Added','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back();  
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function edit($id)
    {
         {
        //$ambulance=Ambulance::where('id',auth()->id())->first();
            $test=test::where('id',$id)->first();

            // dd($ambulance);

        if ($test==null){

            //return 'Post Not found';
            return abort(404);
        }

        // $post=Post::where('author_id',$d)->orWhere('id',$d)->first();
        return view('admin.test.edit')->with('test',$test);

        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $test = test::findOrFail($id);
            
       
        $test->tname =$request->tname;
        $test->tcost =$request->tcost;       
        $test->save();
        
        // Toastr::success('Doctor Successfully Added','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back();  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         {
        $test = test::find($id);

        if (file_exists('image/'.$test->image)) {

            //unlink('image/'.$doctor->image);

        }

        $test->delete();

        // Toastr::success('Ambulance Successfully Deleted','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back(); 
    }
    }
}
