<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use Carbon\Carbon;
class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index()
    {

        $package = Package::all();
         return view('admin.package.index', compact('package'));
    }


       public function visitor()
    {
        $package = Package::all();
         return view('visitor.package', compact('package'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd();
         $package = new package();

        $image = $request->file('image');
        $slug = str_slug($request->npackage);

        if (isset($image)) {

            $currentDate = Carbon::now()->toDateString();
            
            $image_name = $slug.'-'.$currentDate .uniqid(). '.' .$image->getClientOriginalExtension();
            if (!file_exists('image')) {
                mkdir('image', 0777, true);
            }
            $image->move('image',$image_name);
        }

        $package->npackage =$request->npackage;
       // $doctor->user_id = Auth::guard('dashboard')->id();
        $package->tshared =$request->tshared;
        $package->sstandard =$request->sstandard;
        $package->sdelux =$request->sdelux;        
        $package->suit =$request->suit;      
        $package->dhour =$request->dhour;
        $package->remarks =$request->remarks;
        $package->echarge =$request->echarge;       
        $package->image =$image_name;
        $package->save();
        
        // Toastr::success('Doctor Successfully Added','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         {
        //$ambulance=Ambulance::where('id',auth()->id())->first();
            $package=package::where('id',$id)->first();

            // dd($ambulance);

        if ($package==null){

            //return 'Post Not found';
            return abort(404);
        }

        // $post=Post::where('author_id',$d)->orWhere('id',$d)->first();
        return view('admin.package.edit')->with('package',$package);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           $package = package::findOrFail($id);

        $image = $request->file('image');
        $slug = str_slug($request->name);

        if (isset($image)) {

            $currentDate = Carbon::now()->toDateString();
            
            $image_name = $slug.'-'.$currentDate .uniqid(). '.' .$image->getClientOriginalExtension();
            
            unlink('image/'.$package->image);
            $image->move('image/',$image_name);

        }else{
            $image_name = $package->image;
        }

         $package->npackage =$request->npackage;
       // $doctor->user_id = Auth::guard('dashboard')->id();
        $package->tshared =$request->tshared;
        $package->sstandard =$request->sstandard;
        $package->sdelux =$request->sdelux;        
        $package->suit =$request->suit;      
        $package->dhour =$request->dhour;
        $package->remarks =$request->remarks;
        $package->echarge =$request->echarge;       
        $package->image =$image_name;
        $package->save();
        
        // Toastr::success('Doctor Successfully Added','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back();  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = package::find($id);

        if (file_exists('image/'.$package->image)) {

            //unlink('image/'.$doctor->image);

        }

        $package->delete();

        // Toastr::success('Ambulance Successfully Deleted','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back(); 
    }
}
