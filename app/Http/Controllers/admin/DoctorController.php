<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Doctor;
use Carbon\Carbon;
class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $doctor = Doctor::all();
         return view('admin.doctor.index', compact('doctor'));
    }


       public function visitor()
    {
        $doctor = Doctor::all();
         return view('visitor.doctors', compact('doctor'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.doctor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd();
         $doctor = new Doctor();

        $image = $request->file('image');
        $slug = str_slug($request->name);

        if (isset($image)) {

            $currentDate = Carbon::now()->toDateString();
            
            $image_name = $slug.'-'.$currentDate .uniqid(). '.' .$image->getClientOriginalExtension();
            if (!file_exists('image')) {
                mkdir('image', 0777, true);
            }
            $image->move('image',$image_name);
        }

        $doctor->name =$request->name;
       // $doctor->user_id = Auth::guard('dashboard')->id();
        $doctor->email =$request->email;
        $doctor->address =$request->address;
        $doctor->designation =$request->designation;
        $doctor->bd =$request->bd;
        $doctor->degree =$request->degree;      
        $doctor->specilities =$request->specilities;
        $doctor->phone =$request->phone;
        $doctor->schedule =$request->schedule;       
        $doctor->image = $image_name;
        $doctor->save();
        
        // Toastr::success('Doctor Successfully Added','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         {
        //$ambulance=Ambulance::where('id',auth()->id())->first();
            $doctor=doctor::where('id',$id)->first();

            // dd($ambulance);

        if ($doctor==null){

            //return 'Post Not found';
            return abort(404);
        }

        // $post=Post::where('author_id',$d)->orWhere('id',$d)->first();
        return view('admin.doctor.edit')->with('doctor',$doctor);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           $doctor = doctor::findOrFail($id);

        $image = $request->file('image');
        $slug = str_slug($request->name);

        if (isset($image)) {

            $currentDate = Carbon::now()->toDateString();
            
            $image_name = $slug.'-'.$currentDate .uniqid(). '.' .$image->getClientOriginalExtension();
            
            unlink('image/'.$doctor->image);
            $image->move('image/',$image_name);

        }else{
            $image_name = $doctor->image;
        }

        $doctor->name =$request->name;
       // $doctor->user_id = Auth::guard('dashboard')->id();
        $doctor->email =$request->email;
        $doctor->address =$request->address;
        $doctor->designation =$request->designation;
        $doctor->bd =$request->bd;
        $doctor->degree =$request->degree;        
        $doctor->specilities =$request->specilities;
        $doctor->phone =$request->phone;
        $doctor->schedule =$request->schedule;       
        $doctor->image = $image_name;
        $doctor->save();
        
        // Toastr::success('Doctor Successfully Added','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back();  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doctor = doctor::find($id);

        if (file_exists('image/'.$doctor->image)) {

            //unlink('image/'.$doctor->image);

        }

        $doctor->delete();

        // Toastr::success('Ambulance Successfully Deleted','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back(); 
    }
}
