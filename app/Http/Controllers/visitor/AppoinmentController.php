<?php

namespace App\Http\Controllers\visitor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Appoinment;
use Carbon\Carbon;
class AppoinmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $appoinment = Appoinment::all();
         return view('admin.appoinment.index', compact('appoinment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('visitor.appoinment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $appoinment = new Appoinment();

        $image = $request->file('image');
        $slug = str_slug($request->name);

        if (isset($image)) {

            $currentDate = Carbon::now()->toDateString();
            
            $image_name = $slug.'-'.$currentDate .uniqid(). '.' .$image->getClientOriginalExtension();
            if (!file_exists('image')) {
                mkdir('image', 0777, true);
            }
            $image->move('image',$image_name);
        }

        $appoinment->name =$request->name;
       // $doctor->user_id = Auth::guard('dashboard')->id();
        $appoinment->email =$request->email;
        $appoinment->phone =$request->phone;
        $appoinment->gender =$request->gender;
        $appoinment->specialization =$request->specialization;
        $appoinment->date =$request->date;      
        $appoinment->medication =$request->medication;
         
        $appoinment->save();
        
        // Toastr::success('Doctor Successfully Added','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appoinment = appoinment::find($id);

        if (file_exists('image/'.$appoinment->image)) {

            //unlink('image/'.$doctor->image);

        }

        $appoinment->delete();

        // Toastr::success('Ambulance Successfully Deleted','message', ["positionClass" => "toast-bottom-right"]);

        return redirect()->back(); 
    }
}
