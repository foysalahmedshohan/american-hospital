<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seat extends Model
{
	protected $fillable=['stype','tseat','aseat','image','charge'];
    }
